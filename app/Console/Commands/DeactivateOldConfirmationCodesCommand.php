<?php

namespace App\Console\Commands;

use App\Domain\Users\Actions\DeactivateConfirmationCodeAction;
use Illuminate\Console\Command;

class DeactivateOldConfirmationCodesCommand extends Command
{
    /**
     * The name and signature of the console command.
     */
    protected $signature = 'passwords:deactivate-confirmation-codes';

    /**
     * The console command description.
     */
    protected $description = 'Деактивация кодов подтверждения, срок жизни которых истек';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle(DeactivateConfirmationCodeAction $action)
    {
        $action->execute();
    }
}
