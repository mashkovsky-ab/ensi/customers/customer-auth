<?php

namespace App\Console\Commands\Kafka;

use App\Domain\Users\Models\User;
use Ensi\LaravelPhpRdKafkaProducer\HighLevelProducer;
use Illuminate\Console\Command;
use Throwable;

class SendExampleKafkaMessage extends Command
{
    /** @inerhitDoc */
    protected $signature = 'kafka:send-example-message';
    /** @inerhitDoc  */
    protected $description = 'Send a message with default data to'
    . ' <contour>.communication.test.example.1 kafka topic'
    . ' (hint: check your config cache if processor for topic exists and not found)';

    /**
     * Execute the console command.
     */
    public function handle(): int
    {
        $user = User::factory()->create();

        try {
            (new HighLevelProducer(topic('customer-auth.test.example.1')))->sendOne($user->__toString());
        } catch (Throwable $e) {
            $this->error('An error occurred while sending a message to kafka');
            $this->line($e->getMessage());

            return 1;
        }

        $this->line('The message was sent successfully');

        return 0;
    }
}
