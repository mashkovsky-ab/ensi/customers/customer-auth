<?php

namespace App\Http\ApiV1\Modules\Users\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use App\Rules\PhoneRule;

class PasswordResetFirstStepUserRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'phone' => ['nullable', new PhoneRule()],
            'email' => ['nullable'],
        ];
    }
}
