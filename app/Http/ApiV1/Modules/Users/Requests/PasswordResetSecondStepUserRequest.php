<?php

namespace App\Http\ApiV1\Modules\Users\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use App\Rules\PhoneRule;

class PasswordResetSecondStepUserRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'reset_type' => ['required', 'string'],
            'code' => ['required', 'string'],
            'email' => ['nullable', 'string'],
            'phone' => ['nullable', new PhoneRule()],
            'password' => ['required', 'string'],
        ];
    }
}
