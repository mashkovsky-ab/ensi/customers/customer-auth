<?php

namespace App\Http\ApiV1\Modules\Users\Controllers;

use App\Domain\Users\Actions\CreateUserAction;
use App\Domain\Users\Actions\DeleteUserAction;
use App\Domain\Users\Actions\PasswordResetSecondStepUserAction;
use App\Domain\Users\Actions\PasswordResetFirstStepUserAction;
use App\Domain\Users\Actions\PatchUserAction;
use App\Domain\Users\Actions\RefreshPasswordTokenAction;
use App\Http\ApiV1\Modules\Users\Queries\UsersQuery;
use App\Http\ApiV1\Modules\Users\Requests\CreateUserRequest;
use App\Http\ApiV1\Modules\Users\Requests\PasswordResetSecondStepUserRequest;
use App\Http\ApiV1\Modules\Users\Requests\PasswordResetFirstStepUserRequest;
use App\Http\ApiV1\Modules\Users\Requests\PatchUserRequest;
use App\Http\ApiV1\Modules\Users\Resources\UsersResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Http\Request;

class UsersController
{
    public function create(CreateUserRequest $request, CreateUserAction $action)
    {
        return new UsersResource($action->execute($request->validated()));
    }

    public function patch(int $userId, PatchUserRequest $request, PatchUserAction $action)
    {
        return new UsersResource($action->execute($userId, $request->validated()));
    }

    public function delete(int $userId, DeleteUserAction $action)
    {
        $action->execute($userId);

        return new EmptyResource();
    }

    public function get(int $userId, UsersQuery $query)
    {
        return new UsersResource($query->findOrFail($userId));
    }

    public function search(PageBuilderFactory $pageBuilderFactory, UsersQuery $query)
    {
        return UsersResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function searchOne(UsersQuery $query)
    {
        return new UsersResource($query->firstOrFail());
    }

    public function current(Request $request)
    {
        return $request->user() ? new UsersResource($request->user()) : new EmptyResource();
    }

    public function refreshPasswordToken(int $usedId, RefreshPasswordTokenAction $action)
    {
        $action->execute($usedId);

        return new EmptyResource();
    }

    public function passwordResetFirstStep(PasswordResetFirstStepUserRequest $request, PasswordResetFirstStepUserAction $action)
    {
        return new UsersResource($action->execute($request->validated()));
    }

    public function passwordResetSecondStep(PasswordResetSecondStepUserRequest $request, PasswordResetSecondStepUserAction $action)
    {
        return new UsersResource($action->execute($request->validated()));
    }
}
