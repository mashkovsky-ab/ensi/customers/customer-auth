<?php

namespace App\Http\ApiV1\Modules\Users\Queries;

use App\Domain\Users\Models\User;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class UsersQuery extends QueryBuilder
{
    public function __construct(Request $request)
    {
        $query = User::query();

        parent::__construct($query, new Request($request->all()));

        $this->allowedSorts(['id', 'created_at', 'updated_at']);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('login'),
            AllowedFilter::exact('active'),
            AllowedFilter::exact('password_token'),

            AllowedFilter::scope('login_like'),
        ]);

        $this->defaultSort('id');
    }
}
