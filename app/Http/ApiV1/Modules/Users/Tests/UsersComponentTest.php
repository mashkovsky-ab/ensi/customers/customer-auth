<?php

use App\Domain\Kafka\Actions\Send\SendUserUpdatedEventAction;
use App\Domain\Users\Models\User;
use App\Http\ApiV1\Modules\Users\Tests\Factories\UserRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertDatabaseMissing;
use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

// region users:search
test("POST /api/v1/users:search success", function () {
    $users = User::factory()->count(3)->create();
    $filterId = $users->last()->id;

    postJson("/api/v1/users:search", [
        "filter" => ["id" => $filterId],
        "sort" => ["id"],
    ])
        ->assertOk()
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $filterId);
});
test("POST /api/v1/users:search sort success", function (string $sort) {
    User::factory()->create();
    postJson("/api/v1/users:search", ["sort" => ["$sort"]])->assertOk();
})->with(['id', 'created_at', 'updated_at']);
test(
    "POST /api/v1/users:search filter success",
    function (string $fieldKey, $value = null, ?string $filterKey = null, $filterValue = null) {
        /** @var User $order */
        $order = User::factory()->create($value ? [$fieldKey => $value] : []);
        postJson("/api/v1/users:search", ["filter" => [
            ($filterKey ?: $fieldKey) => ($filterValue ?: $order->{$fieldKey}),
        ]])
            ->assertOk()
            ->assertJsonCount(1, 'data')
            ->assertJsonPath('data.0.id', $order->id);
    }
)->with([
        ['login', 'test_login_check', 'login', ['test_login_check', 'test_login_check_one']],
        ['login', 'test_login_check'],
        ['login', 'test_login_check', 'login_like', 't_login'],
        ['active', false],
        ['id', 4, 'id', [4, 5, 6]],
        ['id', 4],
]);
// endregion

// region users:search-one
test("POST /api/v1/users:search-one success", function () {
    $users = User::factory()
        ->count(10)
        ->create();
    /** @var User $requiredUser */
    $requiredUser = $users->last();

    postJson("/api/v1/users:search-one", [
        "filter" => ["id" => $requiredUser->id],
        "sort" => ["id"],
    ])
        ->assertOk()
        ->assertJsonPath('data.id', $requiredUser->id)
        ->assertJsonPath('data.login', $requiredUser->login)
        ->assertJsonPath('data.active', $requiredUser->active);
});
// endregion

// region users/{id}
test("GET /api/v1/users/{id} success", function () {
    /** @var User $user */
    $user = User::factory()->create();

    getJson("/api/v1/users/{$user->id}")
        ->assertOk()
        ->assertJsonPath('data.id', $user->id)
        ->assertJsonPath('data.login', $user->login)
        ->assertJsonPath('data.active', $user->active);
});
test("GET /api/v1/users/{id} 404", function () {
    getJson("/api/v1/users/1")
        ->assertNotFound()
        ->assertJsonPath('data', null)
        ->assertJsonPath('errors.0.code', "NotFoundHttpException");
});

test("PATCH /api/v1/users/{id} success", function () {
    /** @var User $user */
    $user = User::factory()->create();

    $userData = UserRequestFactory::new()->make();

    $this->mock(SendUserUpdatedEventAction::class)->shouldReceive('execute');

    patchJson("/api/v1/users/{$user->id}", $userData)
        ->assertOk()
        ->assertJsonPath('data.id', $user->id)
        ->assertJsonPath('data.login', $userData['login'])
        ->assertJsonPath('data.active', $userData['active']);
    assertDatabaseHas((new User)->getTable(), [
        'id' => $user->id,
        'active' => $userData['active'],
        'login' => $userData['login'],
    ]);
});
test("PATCH /api/v1/users/{id} 400 same login", function () {
    /** @var User $user */
    $user = User::factory()->create();
    $userData = UserRequestFactory::new()->only(['login'])->make(['login' => $user->login]);

    patchJson("/api/v1/users/{$user->id}", $userData)
        ->assertStatus(400)
        ->assertJsonPath('data', null)
        ->assertJsonPath('errors.0.code', "ValidationError");
});
test("PATCH /api/v1/users/{id} 404", function () {
    patchJson("/api/v1/users/1", UserRequestFactory::new()->make())
        ->assertNotFound()
        ->assertJsonPath('data', null)
        ->assertJsonPath('errors.0.code', "NotFoundHttpException");
});

test("DELETE /api/v1/users/{id} success", function () {
    /** @var User $user */
    $user = User::factory()->create();

    deleteJson("/api/v1/users/{$user->id}")->assertOk();
    assertDatabaseMissing((new User)->getTable(), [
        'id' => $user->id,
        'active' => $user->active,
        'login' => $user->login,
    ]);
});
test("DELETE /api/v1/users/{id} 404", function () {
    deleteJson("/api/v1/users/1")->assertNotFound();
});
// endregion

// region users
test("POST /api/v1/users success", function () {
    $userData = UserRequestFactory::new()->make();
    postJson("/api/v1/users", $userData)
        ->assertCreated()
        ->assertJsonPath('data.active', $userData['active'])
        ->assertJsonPath('data.login', $userData['login']);
    assertDatabaseHas((new User)->getTable(), [
        'active' => $userData['active'],
        'login' => $userData['login'],
    ]);
});
test("POST /api/v1/users 400 same login", function () {
    $user = User::factory()->create();
    $newUserData = UserRequestFactory::new()->make();
    $newUserData['login'] = $user->login;
    postJson("/api/v1/users", $newUserData)
        ->assertStatus(400)
        ->assertJsonPath('errors.0.code', "ValidationError");
});
// endregion

// region users/{id}:refresh-password-token
//test("POST /api/v1/users/{id}:refresh-password-token success", function () {
//    /** @var User $user */
//    $user = User::factory()->create();
//
//    postJson("/api/v1/users/{$user->id}:refresh-password-token")
//        ->assertOk();
//});
test("POST /api/v1/users/{id}:refresh-password-token 404", function () {
    postJson("/api/v1/users/1:refresh-password-token")->assertNotFound();
});
// endregion
