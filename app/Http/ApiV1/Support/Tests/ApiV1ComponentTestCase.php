<?php

namespace App\Http\ApiV1\Support\Tests;

use Ensi\LaravelOpenApiTesting\ValidatesAgainstOpenApiSpec;
use Tests\ComponentTestCase;
use Ensi\CustomersClient\Api\CustomersApi;

abstract class ApiV1ComponentTestCase extends ComponentTestCase
{
    use ValidatesAgainstOpenApiSpec;

    protected function getOpenApiDocumentPath(): string
    {
        return public_path('api-docs/v1/index.yaml');
    }

    protected function mockCustomersCustomersApi(): MockInterface|CustomersApi
    {
        return $this->mock(CustomersApi::class);
    }
}
