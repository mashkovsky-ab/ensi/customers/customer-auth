<?php

namespace App\Domain\Users\Observers;

use App\Domain\Kafka\Actions\Send\SendUserUpdatedEventAction;
use App\Domain\Users\Models\User;

class UserObserver
{
    public function __construct(private SendUserUpdatedEventAction $sendUserUpdatedEventAction)
    {
        //
    }

    public function updated(User $user)
    {
        $this->sendUserUpdatedEventAction->execute($user);
    }
}
