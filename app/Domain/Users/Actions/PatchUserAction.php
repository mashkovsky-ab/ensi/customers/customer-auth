<?php

namespace App\Domain\Users\Actions;

use App\Domain\Users\Models\User;

class PatchUserAction
{
    public function execute(int $userId, array $fields): User
    {
        $user = User::findOrFail($userId);
        $user->update($fields);

        return $user;
    }
}
