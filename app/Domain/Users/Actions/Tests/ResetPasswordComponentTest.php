<?php

use App\Domain\Customers\Tests\Factories\CustomerFactory;
use App\Domain\Kafka\Actions\Send\SendPasswordResetFirstEventAction;
use App\Domain\Kafka\Actions\Send\SendPasswordResetSecondEventAction;
use App\Domain\Kafka\Actions\Send\SendUserUpdatedEventAction;
use App\Domain\Users\Models\User;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test("POST /api/v1/users/password-reset-first (by email)", function () {
    /** @var User $user */
    /** @var ApiV1ComponentTestCase $this */

    $user = User::factory()->makeOne();
    $user->save();

    $customerFields = CustomerFactory::new()->make(['user_id' => $user->id]);
    $customerFieldsSearch = CustomerFactory::new()->makeResponseOne(['user_id' => $user->id]);
    $this->mockCustomersCustomersApi()->shouldReceive('createCustomer')
        ->andReturn($customerFields);

    $requestBodyForFirstStep = [
        "email" => $customerFields->getEmail(),
    ];

    $this->mockCustomersCustomersApi()->shouldReceive('searchOneCustomer')
        ->andReturn($customerFieldsSearch);

    $this->mock(SendPasswordResetFirstEventAction::class)->shouldReceive('execute');
    $this->mock(SendUserUpdatedEventAction::class)->shouldReceive('execute');

    postJson('/api/v1/users/password-reset-first', $requestBodyForFirstStep)->assertStatus(200);
});

test("POST /api/v1/users/password-reset-second (by email)", function () {
    /** @var User $user */
    /** @var ApiV1ComponentTestCase $this */

    $user = User::factory()->makeOne();
    $user->save();

    $customerFields = CustomerFactory::new()->make(['user_id' => $user->id]);
    $customerFieldsSearch = CustomerFactory::new()->makeResponseOne(['user_id' => $user->id]);
    $this->mockCustomersCustomersApi()->shouldReceive('createCustomer')
        ->andReturn($customerFields);

    $this->mockCustomersCustomersApi()->shouldReceive('searchOneCustomer')
        ->andReturn($customerFieldsSearch);

    $this->mock(SendPasswordResetSecondEventAction::class)->shouldReceive('execute');
    $this->mock(SendUserUpdatedEventAction::class)->shouldReceive('execute');

    $requestBodyForSecondStep = [
        'reset_type' => 'by_token',
        'code' => $user->password_token,
        'email' => $customerFields['email'],
        'password' => 'newTestPassword',
    ];
    postJson('/api/v1/users/password-reset-second', $requestBodyForSecondStep)->assertStatus(200);
});

test("POST /api/v1/users/password-reset-first (by phone)", function () {
    /** @var User $user */
    /** @var ApiV1ComponentTestCase $this */

    $user = User::factory()->makeOne();
    $user->save();

    $customerFields = CustomerFactory::new()->make(['user_id' => $user->id]);
    $customerFieldsSearch = CustomerFactory::new()->makeResponseOne(['user_id' => $user->id]);
    $this->mockCustomersCustomersApi()->shouldReceive('createCustomer')
        ->andReturn($customerFields);

    $requestBodyForFirstStep = [
        "phone" => $customerFields->getPhone(),
    ];

    $this->mockCustomersCustomersApi()->shouldReceive('searchOneCustomer')
        ->andReturn($customerFieldsSearch);

    $this->mock(SendPasswordResetFirstEventAction::class)->shouldReceive('execute');
    $this->mock(SendUserUpdatedEventAction::class)->shouldReceive('execute');

    postJson('/api/v1/users/password-reset-first', $requestBodyForFirstStep)->assertStatus(200);
});

test("POST /api/v1/users/password-reset-second (by phone)", function () {
    /** @var User $user */
    /** @var ApiV1ComponentTestCase $this */

    $user = User::factory()->makeOne();
    $user->save();

    $customerFields = CustomerFactory::new()->make(['user_id' => $user->id]);
    $customerFieldsSearch = CustomerFactory::new()->makeResponseOne(['user_id' => $user->id]);
    $this->mockCustomersCustomersApi()->shouldReceive('createCustomer')
        ->andReturn($customerFields);


    $this->mockCustomersCustomersApi()->shouldReceive('searchOneCustomer')
        ->andReturn($customerFieldsSearch);

    $this->mock(SendPasswordResetSecondEventAction::class)->shouldReceive('execute');
    $this->mock(SendUserUpdatedEventAction::class)->shouldReceive('execute');

    $requestBodyForSecondStep = [
        'reset_type' => 'by_phone',
        'code' => $user->confirmation_code,
        'phone' => $customerFields['phone'],
        'password' => 'newTestPassword',
    ];
    postJson('/api/v1/users/password-reset-second', $requestBodyForSecondStep)->assertStatus(200);
});
