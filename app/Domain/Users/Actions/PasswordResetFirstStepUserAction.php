<?php

namespace App\Domain\Users\Actions;

use App\Domain\Kafka\Actions\Send\SendPasswordResetFirstEventAction;
use App\Domain\Kafka\Messages\Send\PasswordResetFirstEventMessage;
use App\Domain\Users\Models\User;
use Ensi\CustomersClient\Api\CustomersApi;
use Ensi\CustomersClient\Dto\SearchCustomersRequest;

class PasswordResetFirstStepUserAction
{
    public function __construct(
        protected customersApi $customersApi,
        protected SendPasswordResetFirstEventAction $passwordResetEventAction,
    ) {
    }

    public function execute(array $fields)
    {
        if (empty($fields['phone'])) {
            $user = $this->resetByEmail($fields['email']);
        } else {
            $user = $this->resetByPhone($fields['phone']);
        }

        return $user;
    }

    private function resetByPhone(string $phone)
    {
        $request = new SearchCustomersRequest([
            'filter' => [
                'phone' => $phone,
            ],
        ]);
        $customer = $this->customersApi->searchOneCustomer($request)->getData();

        /** @var User $user */
        $user = User::findOrFail($customer->getUserId());
        $user->generateConfirmCode();
        $user->save();

        $this->passwordResetEventAction->execute($user, $customer, PasswordResetFirstEventMessage::PHONE);

        return $user;
    }

    private function resetByEmail(string $email)
    {
        $request = new SearchCustomersRequest([
            'filter' => [
                'email' => $email,
            ],
        ]);
        $customer = $this->customersApi->searchOneCustomer($request)->getData();

        /** @var User $user */
        $user = User::findOrFail($customer->getUserId());
        $user->generatePasswordToken();
        $user->save();

        $this->passwordResetEventAction->execute($user, $customer, PasswordResetFirstEventMessage::EMAIL);

        return $user;
    }
}
