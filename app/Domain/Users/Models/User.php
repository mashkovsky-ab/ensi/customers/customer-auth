<?php

namespace App\Domain\Users\Models;

use Carbon\Carbon;
use Database\Factories\UserFactory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Laravel\Passport\HasApiTokens;

/**
 * @property array $attributes
 *
 * @property int $id
 * @property string $login
 * @property string|null $password
 * @property bool $active
 * @property string $password_token
 * @property Carbon $password_token_created_at
 * @property string $confirmation_code
 * @property Carbon $confirmation_code_created_at
 *
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class User extends Authenticatable
{
    use HasApiTokens;
    use Notifiable;
    use HasFactory;

    /**
     * Lifetime for confirmation code (in days)
     */
    const LIFE_TIME_PASSWORD_TOKEN = 3;

    /**
     * Lifetime for confirmation code (in days)
     */
    const LIFE_TIME_CONFIRMATION_CODE = 3;

    /**
     * Заполняемые поля модели
     */
    const FILLABLE = ['login', 'password', 'active'];

    /**
     * @var array
     */
    protected $fillable = self::FILLABLE;

    /**
     * @var array
     */
    protected $hidden = ['password'];

    /**
     * @param $value
     */
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = $value ? self::encryptPassword($value) : null;
    }

    /**
     * @param $password
     * @return string
     */
    public static function encryptPassword($password)
    {
        return Hash::make($password);
    }

    /**
     * @param $password
     * @return bool
     */
    public function checkPassword($password)
    {
        return Hash::check($password, $this->password);
    }

    public function generatePasswordToken(): void
    {
        $this->password_token = md5($this->login . time());
        $this->password_token_created_at = now();
    }

    public function destroyPasswordToken(): void
    {
        $this->password_token = null;
        $this->password_token_created_at = null;
    }

    /**
     * Generate new confirmation code for sms
     *
     * @return void
     */
    public function generateConfirmCode(): void
    {
        $this->confirmation_code = substr(str_shuffle('0123456789'), 0, 4);
        $this->confirmation_code_created_at = now();
    }

    /**
     * @param $code
     * @return bool
     */
    public function checkConfirmCode($code)
    {
        return $this->confirmation_code == $code;
    }

    public function destroyConfirmCode(): void
    {
        $this->confirmation_code = null;
        $this->confirmation_code_created_at = null;
    }

    /**
     * Override the field which is used for username in the Laravel Passport authentication
     *
     * @param string $login
     */
    public function findForPassport(string $login)
    {
        return $this->where('login', $login)->first();
    }

    public function scopeLoginLike(Builder $query, $login): Builder
    {
        return $query->where('login', 'ilike', "%{$login}%");
    }

    /**
     * Add a password validation callback for Laravel Passport
     *
     * @param $password
     * @return bool Whether the password is valid
     */
    public function validateForPassportPasswordGrant($password)
    {
        return $this->checkPassword($password);
    }

    /**
     * Find user by email
     *
     * @param string $email
     */
    public static function findByEmail(string $email)
    {
        return User::where('email', $email)->first();
    }

    /**
     * Find user by phone
     *
     * @param string $phone
     */
    public static function findByPhone(string $phone)
    {
        return User::where('phone', $phone)->first();
    }

    /**
     * Find user by password token
     *
     * @param string $email
     */
    public static function findByPasswordToken(string $passwordToken)
    {
        return User::where('password_token', $passwordToken)->first();
    }

    /**
     * Create a new factory instance for the model.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    protected static function newFactory()
    {
        return UserFactory::new();
    }
}
