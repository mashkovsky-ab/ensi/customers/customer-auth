<?php

namespace App\Domain\Users\Models\Tests\Factories;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;

class UserFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->optionalId(),
            'login' => $this->faker->unique()->userName,
            'password' => $this->faker->password(),
            'active' => $this->faker->boolean(),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
