<?php

use App\Domain\Kafka\Messages\Send\UserUpdatedEventMessage;
use App\Domain\Users\Models\User;
use function PHPUnit\Framework\assertEquals;

use function PHPUnit\Framework\assertIsArray;
use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class);
uses()->group('unit');

// region UserUpdatedEventMessage
test("generate UserUpdatedEventMessage success", function () {
    /** @var User $user */
    $user = User::factory()->makeOne();
    $user->save();

    $oldActive = $user->active;
    $user->active = !$oldActive;

    $message = new UserUpdatedEventMessage($user);
    $messageArray = $message->toArray();
    assertIsArray($messageArray);
    assertEquals($messageArray['old_data']['active'], $oldActive);
});
// endregion
