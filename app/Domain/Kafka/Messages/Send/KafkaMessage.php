<?php

namespace App\Domain\Kafka\Messages\Send;

abstract class KafkaMessage
{
    abstract public function toArray(): array;

    abstract public function topicName(): string;
}
