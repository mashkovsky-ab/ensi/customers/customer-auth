<?php

namespace App\Domain\Kafka\Messages\Send;

use App\Domain\Kafka\TopicNameBuilder;
use Ensi\CustomersClient\Dto\Customer;

class PasswordResetSecondEventMessage extends KafkaMessage
{
    public function __construct(private Customer $customer)
    {
    }

    public function toArray(): array
    {
        return [
            'user_full_name' => $this->customer->getFirstName() . ' ' . $this->customer->getLastName(),
            'user_email' => $this->customer->getEmail(),
        ];
    }

    public function topicName(): string
    {
        return TopicNameBuilder::fact('password-reset-second');
    }
}
