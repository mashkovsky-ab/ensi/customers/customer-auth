<?php

namespace App\Domain\Kafka\Actions\Send;

use App\Domain\Kafka\Messages\Send\UserUpdatedEventMessage;
use App\Domain\Users\Models\User;

class SendUserUpdatedEventAction extends SendMessageAction
{
    public function execute(User $user)
    {
        $event = new UserUpdatedEventMessage($user);
        $this->send($event);
    }
}
