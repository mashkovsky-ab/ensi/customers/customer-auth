<?php

namespace App\Domain\Kafka\Actions\Send;

use App\Domain\Kafka\Messages\Send\PasswordResetSecondEventMessage;
use Ensi\CustomersClient\Dto\Customer;

class SendPasswordResetSecondEventAction extends SendMessageAction
{
    public function execute(Customer $customer)
    {
        $event = new PasswordResetSecondEventMessage($customer);
        $this->send($event);
    }
}
