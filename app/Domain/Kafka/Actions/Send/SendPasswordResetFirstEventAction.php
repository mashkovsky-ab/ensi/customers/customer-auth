<?php

namespace App\Domain\Kafka\Actions\Send;

use App\Domain\Kafka\Messages\Send\PasswordResetFirstEventMessage;
use App\Domain\Users\Models\User;
use Ensi\CustomersClient\Dto\Customer;

class SendPasswordResetFirstEventAction extends SendMessageAction
{
    public function execute(User $user, Customer $customer, string $event)
    {
        $event = new PasswordResetFirstEventMessage($user, $customer, $event);
        $this->send($event);
    }
}
